require essioc
require dtlskid
require s7plc
require modbus
require calc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Register IOC db directory
epicsEnvSet(DB_DIR, "$(E3_CMD_TOP)/db/")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/cwm_cws04_ctrl_plc_001.iocsh")
dbLoadRecords("$(DB_DIR)alarm-ilck-thrs.db")

iocshLoad("$(dtlskid_DIR)/dtlskid.iocsh")



#
# SQL State Machine
#
afterInit(seq DtlSkid_communication)
afterInit(seq cooling_statemachine)
